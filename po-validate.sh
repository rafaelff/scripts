#!/bin/bash
#
#    po-validate.sh - checks whether one .po file pass the msgfmt validation
#    Copyright (C) 2018-2019 Rafael Fontenelle <rafaelff@gnome.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>

# FIXME: no support for filename with whitespace

inputlist="$*"
temp=$(mktemp)

if [ $# -lt 1 ]; then
    echo "No input file provided, aborting." > "$temp"
    exit 1
fi

for file in $inputlist; do
    echo "$file"
    msgfmt --statistics -cvvo /dev/null "$file" 2>> "$temp"
    echo "" >> "$temp"
done

gnome-terminal -- sh -c "cat $temp; read; rm $temp"

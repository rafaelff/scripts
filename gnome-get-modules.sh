#!/bin/bash
#
# gnome-get-modules - Get source of translatable modules from GNOME Git
#
# Copyright (C) 2012-2019 Rafael Fontenelle <rafaelff@gnome.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#

# folder that will store, or does store the downloaded modules
modsfolder=~/GNOME

# function to quit with error message
die() {
    echo "$1" >&2
    exit 1
}

# Main function that enters each modules folder and trigger all other
# functions in this script
main() {
  if [ ! -x /usr/bin/git ]; then
    die "Git executable not found. Is it installed?"
  fi
  [ -d $modsfolder ] || mkdir $modsfolder
  cd "$modsfolder" || die "Failed to access the modules folder $modsfolder."

  dnssecverify

  echo -e "\n*** Entered in GNOME modules directory \"$modsfolder\" ***\n"
  mod=$(listmodules)
  updateallmodules "$mod"
}

# Ensure that we are not victim of a Man In The Middle (MITM) Attack, but
# verifying DNS connection via DNSSEC. This is possible since:
# https://mail.gnome.org/archives/infrastructure-announce/2013-November/msg00000.html
dnssecverify() {
  output=$(delv gnome.org. A | grep '^; fully validated$')
  if [ -z "$output" ]; then
    die "Failed while verifying gnome.org. Man In The Middle attack?"
  fi
}

# Fetch a list of translateable modules from Damned Lies' modules page
# by filtering it and extracting the modules name from the href
#
# parameters: none
# returns: a one-lined list of modules whitespace-separated
listmodules() {
  tmpfile=$(mktemp)
  wget https://l10n.gnome.org/module/ -O "$tmpfile" -o /dev/null
  status=$?
  if [ $status -ne 0 ]; then
    die "Wget failed with error status $status. Aborting."
  fi
  modules="$(grep "href=\"/module/" "$tmpfile" | grep -v '<span>' | cut -d/ -f3 | tr "\n" " ")"
  rm "$tmpfile"
  modules=$(filtermodules "$modules")
  echo "$modules"
}


# Some of the so called modules actually doesn't exist or are bugged or
# duplicated, so that requires some intervention. This script removes
# these specific modules for the module list, resulting in a cleaner,
# less bugged list.
#
# parameters: a one-lined list of modules whitespace-separated
# returns: a one-lined list of modules whitespace-separated, fixed
filtermodules() {
  tmpfile=$(mktemp)

  echo -n "$1" > "$tmpfile"

  # Available in Damned Lies, but do not use GNOME Git repo; can't download
  removelist="accountsservice appstream appstream-glib aravis avahi colord cups-pk-helper flatpak fprintd fwupd gst-plugins-bad gst-plugins-base gst-plugins-good gst-plugins-ugly gstreamer netspeed NetworkManager p11-kit packagekit polkit PulseAudio realmd shared-mime-info systemd udisks webkit xdg-desktop-portal xdg-desktop-portal-gtk xdg-user-dirs xkeyboard-config "

  # Deprecated or group of modules that cannot be found anymore in Git repository
  removelist="$removelist contacts ease evolution-exchange evolution-webcal fantasdic galeon gcalctool gimp-help-2 gnome-games-collection gnome-lirc-properties gnome-media gnome-system-tools gnome-themes gnome-utils gnome-vfs gnucash gtkhtml gyrus hamster-applet kupfer libgnomecanvas libgnomeprint libgnomeprintui monkey-bubble pan2 pessulus sabayon the-board Gtk-properties Gtk-UI"

  # These don't show in fetched list of modules, but they should be available
  includelist="gimp-help pan"

  ## Debug variables
  #echo -e "All modules: $(cat $tmpfile)\n" >&2

  for removemod in $removelist; do
    sed -i "s/ $removemod / /" "$tmpfile"
  done

  echo -n " $includelist" >> "$tmpfile"

  #echo -e "To be removed: $removelist" >&2
  echo -e "TARGET MODULES:\n$(cat "$tmpfile")\n" >&2
  cat "$tmpfile"
}

# For a given list of modules, enter in their respective folder and
# run git clone to update all modules. Case module doesn't existe,
# it tries to clone the Git repo. Failed modules will be displayed
# for the end-user. The master branch was chosen as default, because
# there is a big variety of branches throughout the git repositories.
#
# parameters: a list of modules whitespace-separated
# returns: list of failed updates attempts, if any
updateallmodules() {
  cloneurl="git@gitlab.gnome.org:GNOME"
  modules="$*"
  failed=""
  # makes 'git up' alias
  git config --global alias.up '!git pull --rebase && git submodule update --init'

  for mod in $modules; do
    echo -e "\n*** Working on module \"$mod\" ***\n"

    if [ ! -d "$mod" ]; then
      git clone "$cloneurl/$mod.git" || failed+="$mod "
    fi
    if [ -d "$mod" ]; then
      cd "$mod" || continue

        # migrate from git.gnome.org to gitlab.gnome.com
        # https://mail.gnome.org/archives/infrastructure-announce/2018-May/msg00001.html
        if [ -f .git/config ]; then
            if grep -q "@git.gnome.org/git/$mod" .git/config; then
                git remote set-url origin "git@gitlab.gnome.org:GNOME/$mod.git"
            fi
        fi

      git checkout master && git up || failed+="$mod "
      cd ..
    fi
  done
  if [ -n "$failed" ]; then
    echo "ERROR: Done, but failed to get updated source code for the following modules:"
    echo "   $failed"
  fi
}

# Run all the code above by calling main function
main

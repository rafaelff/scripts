#!/bin/sh
#
#    rerankmirrors - Reorder Arch mirrors with best times first
#    Copyright (C) 2018-2021  Rafael Fontenelle
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

if ! test -x /usr/bin/wget; then
    echo "'wget' not found but required for downloading mirrorlist"
    echo "Please make sure 'wget' package is installed"
    echo ""
    exit 1
fi

if ! test -x /usr/bin/rankmirrors; then
    echo "'rankmirrors' not found but required for ranking the mirrorlist"
    echo "Please make sure 'pacman-contrib' package is installed"
    echo ""
    exit 1
fi


arqtmp=$(mktemp)
ranked=$(mktemp)

wget -q -O "$arqtmp" "https://archlinux.org/mirrorlist/?country=BR&protocol=http&protocol=https&ip_version=4&use_mirror_status=on"

sed -i 's/^#Server/Server/' "$arqtmp"

rankmirrors --verbose "$arqtmp" | tee "$ranked"

comment="#Uncomment this to revert to a previous state of packages."
date=$(date --date="yesterday" +'%Y/%m/%d')
line="#Server = https://archive.archlinux.org/repos/$date/\$repo/os/\$arch"
sed -i "1s;^;${comment}\n${line}\n\n;" "$ranked"

# if sudo is available, update mirrorlist;
# otherwise, don't update and tell the user.
if test -x /usr/bin/sudo; then

    sudo install -Dm644 -oroot -groot "$ranked" /etc/pacman.d/mirrorlist
    echo ""
    echo "Successfully updated /etc/pacman.d/mirrorlist"
    echo ""

else
    echo ""
    echo "info: mirrorlist NOT updated yet, because 'sudo' was not found!"
    echo "info: please move the file \"$ranked\" to /etc/pacman.d/mirrorlist"
    echo ""
  
fi

#!/bin/bash
#    fix-pot-typography.sh - apply GNOME HIG recomendations on Unicode
#                            chars to POT files, to help highlighting
#                            what string in the source code should have
#                            typography changed
#
#    Copyright (C) 2012-2019 Rafael Fontenelle <rafaelff@gnome.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
#
# See:     https://developer.gnome.org/hig/stable/typography.html
#
# Summary: “ ” ∶ × … ‘ ’ • –

die() {
    echo "$1" 2>&1
    exit 1
}

[ $# -eq 0 ] && die "No argument provided, expected a GNU Gettext POT file."
[ ! -f "$1" ]  && die "$1 must be a valid GNU Gettext POT filename."
[[ "$1" != *.pot ]] && die "Filename is not a GNU Gettext POT, but a POT is expected."

[ ! -x "$(command -v msgcat)" ] && die "Gettext not installed but required."
[ ! -x "$(command -v meld)" ] && die "Meld not installed but required."
[ ! -x "$(command -v sed)" ] && die "Sed not installed but required."

oldpot=$1
newpot=${oldpot}-new
tmpfile=$(mktemp)

cp -a "$oldpot" "$newpot"

msgcat --no-wrap "$newpot" > "$tmpfile"
mv "$tmpfile" "$newpot"

# ratio
# TODO: improve
#sed -i "$newpot"                           \
#    -e '/^msgid/s/:/∶/g'

# multiplication sign
# TODO: implement x->× replacement between numbers

# bullet
sed -i "$newpot"                         \
    -e '/^msgid/s/^\"- /\"• /'           \
    -e '/^msgid/s/^\" - /\" • /'         \

# horizontal ellipsis
sed -i "$newpot"                         \
    -e '/^msgid/s/\.\.\./…/g'            \

# left single quotation mark
sed -i "$newpot"                         \
    -e "/^msgid/s/ '/ ‘/g"               \
    
# right single quotation mark
sed -i "$newpot"                         \
    -e "/^msgid/s/'s/’s/g"               \
    -e "/^msgid/s/s'/s’/g"               \
    -e "/^msgid/s/n't/n’t/g"             \
    -e "/^msgid/s/' /’ /g"               \

# left double quotation mark
sed -i "$newpot"                         \
    -e '/^msgid/s/ \\"/ “/g'             \
    -e '/^msgid/s/^\"\\"/\"“/g'          \
    -e 's/^msgid \"\\"/msgid \"“/g'      \
    -e '/^msgid/s/(\\"/(“/g'             \

# right double quotation mark
sed -i "$newpot"                         \
    -e '/^msgid/s/\\" /” /g'             \
    -e '/^msgid/s/\\"\"$/”\"/g'          \
    -e '/^msgid/s/\\":/”:/g'             \
    -e '/^msgid/s/\\"∶/”∶/g'             \
    -e '/^msgid/s/\\";/”;/g'             \
    -e '/^msgid/s/\\",/”,/g'             \
    -e '/^msgid/s/\\"\./”\./g'           \
    -e '/^msgid/s/)\\"/)”/g'             \
    -e '/^msgid/s/\\")/”)/g'             \
    -e '/^msgid/s/\\"(/”(/g'             \
    -e '/^msgid/s/\\"?/”?/g'             \
    -e '/^msgid/s/\\"\\n/”\\n/g'         \
    -e '/^msgid/s/\\"…/”…/g'             \

msgcat "$newpot" > "$tmpfile"
mv "$tmpfile" "$newpot"

msgcat "$oldpot" > "$tmpfile"
mv "$tmpfile" "$oldpot"

meld "$oldpot" "$newpot"


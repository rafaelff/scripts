#!/bin/bash
#
#   translate-html - manages translation of HTML files
#   Copyright (C) 2017-2019  Rafael Fontenelle
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

program=${0##*\/}

function usage () {
cat <<EOF
Usage: $program [option]... <command> <htmlname>

  This script makes it easier to translate HTML files for other languages
  by providing conversion from/to Gettext message catalogs (.po files), and
  therefore avoiding the need to translate it directly in the HTML file.

  Extra feature is the conversion of translation files to another formats.
  if you want to. So besides the HTML format, the following formats are
  currently supported: Markdown, ODT, PDF.

  All the operations will take place in current directory by default.
  This behaviour can be changed via options (see below) to define the input,
  po and/or output directories. The input directory must exist and have the
  source HTML file, but the po and output directory will be created, if it
  doesn't exist already.

COMMANDs:
  
  Toggle the action you want from this script. It will be ignored if
  the -h option is provided.
  
  Valid values are:
  
  t, translate    generate, or update if existent, the .po file for
                  translation. Use this when want to start a translation
                  of a given HTML file
  b, build        convert the .po file to the translated HTML file, and
                  in the extra file formats enabled

HTMLNAME:
  
  Filename of the source HTML file that will be used to extract strings
  from - and generate .po files - and to convert the .po file to the
  destination files; it will be ignored if the -h option is provided.
  
Options:
  -l language
      the language code of the target files; if omitted, uses \$LANG

  -p podir
      directory to store, and read from, the .po file, for translation;
      if omitted, uses "po" in the current directory ($(pwd)/po)

  -o outputdir
      directory to store the translated files, which includes the
      resulting HTML files and the other extra formats enabled;
      if omitted, uses output in the current directory ($(pwd)/output)

  -M
      disable conversion to Markdown text (default: enable);
      this is ignored when using the 'translated' command

  -O
      disable conversion to ODT (default: enable); this is ignored
      when using the 'translated' command

  -P
      disable conversion to PDF (default: enable); this is ignored
      when using the 'translated' command

  -h
      show this help and exit

Report bugs to <rafaelff@gnome.org>
EOF
}

 # echo and exit commands together in one invocation
die() {
    echo "$1" 2>&1
    exit 1
}

 # initialize the disable options with 0, which means leave enable
disable_odt=0
disable_pdf=0
disable_md=0

 # Set default language value
language=${LANG%.*}

 # Set default working directories
podir=po
outputdir=output

# Parse the command line.
OPTIND=1
while getopts "hvl:p:o:MOP" opt; do
  case "$opt" in
     h ) usage; exit 0 ;;
     l ) language="$OPTARG" ;;
     p ) podir="$OPTARG" ;;
     o ) outputdir="$OPTARG" ;;
     M ) disable_md=1 ;;
     O ) disable_odt=1 ;;
     P ) disable_pdf=1 ;;
     * ) usage; exit 1 ;;
  esac
done
shift $((OPTIND-1))
[ "$1" = "--" ] && shift

[ $# -eq 2 ] || die "Expected TWO files, for command and htmlname, but $# found"
command=$1
htmlname=$2

# check the command provided
if [ "$command" != 'translate' ] && [ "$command" != 't' ] && \
   [ "$command" != 'build' ]     && [ "$command" != 'b' ]; then
    die "Unsupported command \"$command\". Valid commands are: translate, t, build, b"
fi

# check and validate language
[ -n "$language" ] && die "Language not given and unable to determine your locale"
# List all possible languages.
if [ ! -f all-languages ] || [ ! -s all-languages ] || [ ! -r all-languages ]; then
    wget https://www.gnu.org/server/gnun/languages.txt
    awk '!/^#/ {ORS=" "; print $1}' languages.txt > all-languages
fi
# Check that the language provided is in the list.
language=${language/_/-} && language=${language,,}
[[ "$(< all-languages)" =~ $language ]] || die "Unknown language \"$language\"."

# check, create and verify readability/writability of podir, and set absolute path
podir=${podir%%/}
[ "${podir##/}" != "$podir" ] || podir="$(pwd)/$podir"
[ -d "$podir" ] || mkdir -p "$podir" || die "Unable to create \"$podir\""
[ -r "$podir" ] || die "Directory \"$podir\" not readable"
[ -w "$podir" ] || die "Directory \"$podir\" not writable"

# check, create and verify readability/writability of outputdir, and set absolute path
outputdir=${outputdir%%/}
[ "${outputdir##/}" != "$outputdir" ] || outputdir="$(pwd)/$outputdir"
[ -d "$outputdir" ] || mkdir -p "$outputdir" || die "Unable to create \"$outputdir\""
[ -r "$outputdir" ] || die "Directory \"$outputdir\" not readable"
[ -w "$outputdir" ] || die "Directory \"$outputdir\" not writable"


# check existence and validate the source HTML name
if [ ! -f "$htmlname" ]; then
    if [ ! -f "$htmlname.html" ]; then
        die "HTML file \"$htmlname\" neither \"$htmlname.html\" not found"
    else
        htmlname="$htmlname.html"
    fi
fi
[ -r "$htmlname" ] || die "HTML file \"$htmlname\" not readable"
# .po file was provided ?
[ "${htmlname%%.po}" != "$htmlname" ] && die "HTML expected, but got a .po file \"$htmlname\""
# TODO: Is htmlname a HTML file ?


if [ "${htmlname##/}" == "$htmlname" ]; then
    inhtml="$htmlname"
else
    inhtml="$(pwd)/$htmlname"
fi

# get HTML filename without path and extension
core=$(basename "${htmlname%.html}")
po="$podir/$core.$language.po"
out="$outputdir/$core.$language"

if [[ $command == 'translate' ]] || [[ $command == 't' ]] ; then

    if [ ! -f "$po" ]; then
        # creates .po from source HTML file
        PO4A_GETTEXTIZE=$(command -v po4a-gettextize) || die "po4a-translate not found, but required"
        $PO4A_GETTEXTIZE -f xhtml -M utf-8 -o porefs=none -o ontagerror=silent -m "$inhtml" -p "$po"
        sed -i '/^"Content-Type/s/CHARSET/UTF-8/' "$po"
    else
        # updates .po from source HTML file
        PO4A_UPDATEPO=$(command -v po4a-updatepo) || die "po4a-updatepo not found, but required"
        $PO4A_UPDATEPO -f xhtml -M utf-8 -o porefs=none -o ontagerror=silent -m "$inhtml" -p "$po" --previous
    fi

    echo "PO file ready for translation: $po"

elif [[ $command == 'build' ]] || [[ $command == 'b' ]]; then

    # initialize
    summary=

    # check .po file existence before using it
    [ -f "$po" ] || die "PO file \"$po\" not found, but required"

    # check PO4A_TRANSLATE and generate translated HTML from the .po file
    if PO4A_TRANSLATE=$(command -v po4a-translate); then
        if $PO4A_TRANSLATE -f xhtml -M utf-8 -L utf-8 -k 0 -o ontagerror=silent -p "$po" -m "$inhtml" -l "$out.html"; then
            summary="  HTML:     $out.html\n"
        else
            die "Failed while creating translated HTML file $out.html"
        fi
    else
        die "po4a-translate not found, but required"
    fi

    # check PANDOC and convert from translated HTML to Markdown text
    if [ $disable_md -eq 0 ]; then
        if PANDOC=$(command -v pandoc); then
            if $PANDOC -f html -t markdown --atx-headers -o "$out.md" "$out.html"; then
                summary="$summary  Markdown: $out.md\n"
            else 
                echo "Failed while creating translated Markdown file $out.md"
            fi
        else
            # just warn, don't exit
            echo "Pandoc not found, disabled"
        fi
    fi

    # check LIBREOFFICE and convert from translated HTML to ODT and/or PDF
    if [ $disable_odt -eq 0 ] || [ $disable_pdf -eq 0 ]; then
        if LIBREOFFICE=$(command -v libreoffice); then
            if [ $disable_odt -eq 0 ]; then
                if $LIBREOFFICE --headless --convert-to odt --outdir "$outputdir" "$out.html"; then
                    summary="$summary  ODT:      $out.odt\n"
                else
                    echo "Failed while creating translated OpenDocument file $out.odt" 
                fi
            fi

            if [ $disable_pdf -eq 0 ]; then
                if $LIBREOFFICE --headless --convert-to pdf --outdir "$outputdir" "$out.html"; then
                    summary="$summary  PDF:      $out.pdf\n"
                else
                    echo "Failed while creating translated PDF file $out.pdf"
                fi 
            fi
        else
            # just warn, don't exit
            echo "libreoffice not found, disabling ODT and PDF..."
        fi
    fi

    echo ""
    echo "Translated files available:"
    echo -e "$summary"

fi

#!/bin/bash
# Based on Alexandre Franke's contribution in 2014 in:
# https://mail.gnome.org/archives/gnome-doc-list/2014-September/msg00075.html
#
#    po2mallard - builds GNOME help files in a specific language.
#    Copyright (C) 2019-2021  Rafael Fontenelle
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

outdir=page

usage() {
    cat << EOM
Usage:  po2mallard.sh [-h|--help]

  Builds GNOME help files in a specific language.

  Run this from the directory your .po file is in, and the help files
  will be generated in the '$outdir' directory. If a 'legal.xml' file is
  part of the documentation, it will added to the directory as well.
EOM
    exit 0
}

die() {
    echo "$1" >&2
    exit 1
}

if [ $# -gt 0 ]; then
   if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
      usage
   else
      echo "Illegal option \"$1\""
      usage
   fi
fi 

MSGFMT=$(command -v msgfmt)   || die "msgfmt not found; gettext installed?"
ITSTOOL=$(command -v itstool) || die "itstool not found; is it installed?"

lang=$(basename "$(pwd)")
[ -f "$lang.po" ] || die "$lang.po not found; wrong directory?"
[ -d "$outdir" ] || rm -rf "$outdir"

mkdir -vp "$outdir"

$MSGFMT -co "$lang.mo" "$lang.po" || die "msgfmt failed; please fix it."
for page in ../C/*.page; do
  if $ITSTOOL -m "$lang.mo" -o "$outdir" "$page"; then
    echo "Generating ${page/..\/C\/}..."
  else
    echo "Failed to generate ${page/..\/C\/}."
  fi
done

# Copy legal.xml file if any file requires it; yelp fails if it is missing
if grep -R legal.xml ../C/*.page > /dev/null; then
  cp ../C/legal.xml "$outdir"
fi
rm "$lang.mo"

#!/bin/bash
#
#    po-sed-old-limes - Remove lines of old strings from Gettext PO files
#    Copyright (C) 2018-2019 Rafael Fontenelle <rafaelff@gnome.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>

while [ $# -ge 1 ]; do
    if [ "${1%%.po}" == "$1" ]; then
        echo "\"$1\" not a po file, ignoring."
    elif [ ! -f "$1" ]; then
        echo "\"$1\" not found, ignoring."
    else
        if [ -z "$inputlist" ]; then
            inputlist=("$1")
        else
            inputlist+=("$1")
        fi
    fi
    shift
done

sed -i '/^#| /d;/^# | /d' "${inputlist[@]}"

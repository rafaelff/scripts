#!/bin/bash
# 
# zabbix-po-download - get translations files for Zabbix
#
# Copyright (C) 2020 Rafael Fontenelle <rafaelff@gnome.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#

LANGUAGE=pt_BR
URL_PROJECT="https://www.zabbix.com/translate"
ROOTDIR=$HOME/l10n
PODIR=zabbix-po-files
OUTFILE=zabbix-translations


die() {
    echo "$1" >&2
    exit 1
}


for binary in aria2c msgcat msgattrib po2tmx curl unzip; do
    if [ ! -x "$(command -v $binary 2> /dev/null)" ]; then
        die "$binary not found but required. Please install it. Exiting..."
    fi
done

if [ ! -d "$ROOTDIR" ]; then
    mkdir "$ROOTDIR" || die "Unable to make and enter the rootdir $ROOTDIR"
fi

if [ -d "$ROOTDIR"/$PODIR ]; then
    rm -rf "${ROOTDIR:?}"/$PODIR
fi

mkdir -p "$ROOTDIR"/$PODIR
cd "$ROOTDIR"/$PODIR || die "Unable to enter the podir $PODIR (rootdir $ROOTDIR)"


echo -e "Let's get Zabbix PO files!"

# These are all the components to be downloaded
projects="Zabbix-4.0 Zabbix-5.0 Zabbix-5.4 Zabbix-trunk"

touch todownloadlist

for p in $projects; do
    url=$URL_PROJECT/$LANGUAGE/$p/export/zip
    {
        echo "$url"
        echo "  out=${p}-${LANGUAGE}.zip"
        echo ""
    } >> todownloadlist
done

echo "Downloading Zip files containing PO files... "
aria2c -i todownloadlist

echo "Extracting zip files downloaded... "
for p in $projects; do
    unzip -p "${p}-${LANGUAGE}.zip" > "${p}.po" && \
    rm "$p-$LANGUAGE.zip"
done

echo "Converting charset from ISO 8859-1 to UTF-8... "
for pofile in ./*.po; do
    encoding=$(file -bi "$pofile" | sed -e 's/.*[ ]charset=//')
    if [ "$encoding" == iso-8859-1 ]; then
        echo "    converting $pofile..."
        iconv -f iso-8859-1 -t utf-8 "$pofile" > "$pofile-aux"
        mv "$pofile-aux" "$pofile"
        sed -i '/Content-Type/s/ISO-8859-1/UTF-8/' "$pofile"
        sed -i '/Content-Type/s/iso-8859-1/UTF-8/' "$pofile"
    fi
done

echo "Applying no-wrap lines... "
for po in ./*.po; do
    msgcat --no-wrap "$po" > tmp && \
    mv tmp "$po"
done

echo "Combining PO files into one big PO file... "
msgcat ./*.po | \
msgattrib --no-fuzzy --no-obsolete --translated --no-wrap --no-location -o "$ROOTDIR"/$OUTFILE.po

echo "Cleaning header and set proper header the big PO file... "
sed -i '0,/^$/d' "$ROOTDIR"/$OUTFILE.po    # cleanup header lines
cp "$ROOTDIR"/$OUTFILE.po{,-bkp}
cat > header-file << EOM
msgid ""
msgstr ""
"Project-Id-Version: $OUTFILE\n"
"POT-Creation-Date: $(date -u +'%F %T%z')\n"
"PO-Revision-Date: $(date +'%F %T%z')\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: ${LANGUAGE} <LL@li.org>\n"
"Language: ${LANGUAGE}\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

EOM
cat header-file "$ROOTDIR"/$OUTFILE.po-bkp > "$ROOTDIR"/$OUTFILE.po
rm header-file "$ROOTDIR"/$OUTFILE.po-bkp

# Provides faster importing to Machine Translation
echo "Converting PO to TMX... "
po2tmx --language=$LANGUAGE -i "$ROOTDIR"/$OUTFILE.po -o "$ROOTDIR"/$OUTFILE.tmx

echo "$OUTFILE.po/.tmx is stored in $ROOTDIR"

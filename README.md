# Scripts

Copyright (C) 2019 Rafael Fontenelle

In this repository you'll find some of the shell scripts I use, mostly for
helping translation/localization (l10n)

## All scripts available

Below are available the script names and their description (first and second
columns, respectively), divided by their subject (bold text).

<table>

  <!--   Section divider   -->
  <tr>
    <td colspan="2">
        <b>Arch Linux</b>
    </td>
  </tr>
  
  <tr>
    <td>
        archwiki-chkpagelinks.sh
    </td>
    <td>
        Reports translated ArchWiki pages linking against English pages,
        in order to ease the task of identifying and fixing them.
    </td>
  </tr>
  
  <tr>
    <td>
        rerankmirrors.sh
    </td>
    <td>
        Download a mirrorlist file from
        [archlinux.org](https://www.archlinux.org) for a specific language,
        use `rerankmirrors` to rank them for the best mirrors first, and
        install it for use.
    </td>
  </tr>
  
  <tr>
    <td>
        lysetup.sh
    </td>
    <td>
        Set up [LilyPond](http://lilypond.org/)'s source code for building
        web pages and other docs in an Arch Linux system, also providing
        build command-lines and quick links for translation
    </td>
  </tr>

  <!--   Section divider   -->
  
  <tr>
    <td colspan="2">
        <b>Downloaders of projects' PO files for
        [Translation Memory](https://en.wikipedia.org/wiki/Translation_memory)</b>
    </td>
  </tr>

  <tr>
    <td>
        django-compendium.sh
    </td>
    <td>
        Pull translations files from [Django](https://www.djangoproject.com/)'s
        [translation project on Transifex](https://www.transifex.com/django/)
        and create a compendium. Clones the related git repositories on the
        process.
    </td>
  </tr>

  <tr>
    <td>
        fdroid-po-download.sh
    </td>
    <td>
        Download PO tarballs from [F-Droid](https://f-droid.org)'s
        [Weblate](https://hosted.weblate.org/projects/f-droid/) instance,
        store them and create one big PO file for fast queries and to feed
        PO editors' Translation Memory database.
    </td>
  </tr>

  <tr>
    <td>
        gnome-get-modules.sh
    </td>
    <td>
        Clone git repositories of translatable
        [GNOME](https://www.gnome.org)'s modules. List translatable modules
        from [Damned Lies](https://l10n.gnome.org) and git-clone/pull them
        in order to be available offline.
    </td>
  </tr>
  
  <tr>
    <td>
        gnome-po-download.sh
    </td>
    <td>
        Download PO tarballs from [GNOME](https://www.gnome.org)'s
        [Damned Lies](https://l10n.gnome.org),
        store them and create one big PO file for fast queries and to feed
        PO editors' Translation Memory database.
    </td>
  </tr>
  
  <tr>
    <td>
        gnuweb-po-download.sh
    </td>
    <td>
        Look for PO files for GNU Web translation project in a
        [GNUN](https://www.gnu.org/software/gnun/)'s
        [statistics report page](https://www.gnu.org/software/gnun/reports/report-pt-br.html)
        and download them, store them and create one big PO file for fast
        queries and to feed PO editors' Translation Memory database.
    </td>
  </tr>
  
  <tr>
    <td>
        isocodes-po-download.sh
    </td>
    <td>
        Download PO files for
        [ISO Codes](https://salsa.debian.org/iso-codes-team/iso-codes) from its
        [Weblate translation project](https://hosted.weblate.org/projects/iso-codes),
        store them and create one big PO file for fast queries and to feed
        PO editors' Translation Memory database.
    </td>
  </tr>
  
  <tr>
    <td>
        licencas-sl-po-download.sh
    </td>
    <td>
        Copy PO files from existent clone of the
        [licencas.gitlab.io](https://gitlab.com/licencas/licencas.gitlab.io)
        repository, store them and create one big PO file for fast queries
        and to feed PO editors' Translation Memory database.
    </td>
  </tr>
  
  <tr>
    <td>
        translationproject-po-download.sh
    </td>
    <td>
        Search [Translation Project](https://translationproject.org) for
        PO files of a language team, download them, store them and create
        one big PO file for fast queries and to feed PO editors'
        Translation Memory database.
    </td>
  </tr>
  
  <tr>
    <td>
        weblate-po-download.sh
    </td>
    <td>
        Download PO tarballs from [Weblate](https://weblate.org)'s
        [Weblate](https://hosted.weblate.org/projects/f-droid/) instance,
        store them and create one big PO file for fast queries and to feed
        PO editors' Translation Memory database.
    </td>
  </tr>
  
  <tr>
    <td>
        xfce-po-download.sh
    </td>
    <td>
        Search [Xfce's Git repository](https://git.xfce.org) for PO files,
        download them, store them and create one big PO file for fast queries
        and to feed PO editors' Translation Memory database.
    </td>
  </tr>
  
  <tr>
    <td>
        zabbix-po-download.sh
    </td>
    <td>
        Download PO files from [Zabbix](https://www.zabbix.com/)'s
        [Pootle](https://zabbix.org/pootle) instance, store them and
        create one big PO file for fast queries and to feed PO
        editors' Translation Memory database.
    </td>
  </tr>

  <!--   Section divider   -->
  
  <tr>
    <td colspan="2">
        <b>PO file fixers</b>
    </td>
  </tr>
  
  <tr>
    <td>
        fix-typography.sh
    </td>
    <td>
        Edit PO files to take advantage of Unicode. In other words, edit
        them replacing ASCII characters with better-looking Unicode
        characters in conformity with GNOME's
        [Human Interface Guideline (HIG) Typography](https://developer.gnome.org/hig/stable/typography.html).
        After the edition, a graphical diff is invoked to revise the changes.
    </td>
  </tr>
  
  <tr>
    <td>
        fix-pot-typography.sh
    </td>
    <td>
        Similar as above, but it applies to POT file. It is useful to highlight
        which strings that should take advantage of Unicode, which can help
        [BGO#772263](https://bugzilla.gnome.org/show_bug.cgi?id=772263)
    </td>
  </tr>

  <tr>
    <td>
        po-no-wrap.sh
    </td>
    <td>
        Remove line wrap from PO files, to ease grepping strings.
    </td>
  </tr>

  <tr>
    <td>
        po-sed-oldlines.sh
    </td>
    <td>
        Remove old translation lines (prefixed with <code>#| </code>)
        from PO files. See
        [The Format of PO Files](https://www.gnu.org/software/gettext/manual/gettext.html#PO-Files)
        in GNU Gettext Manual.
    </td>
  </tr>

  <tr>
    <td>
        po-wrap.sh
    </td>
    <td>
        Exact opposite of <em>po-no-wrap.sh</em>; applies line wrap to PO files.
    </td>
  </tr>

  <tr>
    <td>
        po-validate.sh
    </td>
    <td>
        Validates PO files using msgfmt; meant to be used as context menu
        item, therefore installed in e.g. ~/.local/share/nautilus/scripts
    </td>
  </tr>

  <!--   Section divider   -->

  <tr>
    <td colspan="2">
        <b>Builders</b>
    </td>
  </tr>

  <tr>
    <td>
        po2mallard.sh
    </td>
    <td>
        Builds GNOME help files of a specific language. Useful for testing
        translation to help files Mallard-based.
    </td>
  </tr>

  <tr>
    <td>
        setup-gnu-licenses.sh
    </td>
    <td>
        Download GNU licenses and prepare them for translating, in
        conformity with the article
        [Unofficial Translations](https://www.gnu.org/licenses/translations.html).
    </td>
  </tr>

  <tr>
    <td>
        translate-html
    </td>
    <td>
        Make it easier to localize HTML file by using html/po conversion;
        output localized HTML, Markdown, ODT and PDF document. Perfect for
        translating GNU licenses, right after using
        <em>setup-gnu-licenses.sh</em>.
    </td>
  </tr>

</table>

## License

All scripts found here are licensed under
[GNU GPLv3+](https://www.gnu.org/licenses/gpl.html). See [LICENSE](LICENSE) file

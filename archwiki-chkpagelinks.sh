#!/bin/bash
#
# archwiki-chkpagelinks - reports Archwiki pages with links to be fixed
#
# Copyright (C) 2019 Rafael Fontenelle <rafaelff@gnome.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
prog=${0##*\/}

usage() {
    cat <<EOM
Usage: $prog [OPTIONS]
Reports translated Archwiki pages linking against English pages, in order
to ease the task of identifying and fixing them.

OPTIONS:
  -d DIR      root dir of the translated Arch Wiki pages (default: current)
  -p PAGE     verify only the given PAGE for links of other pages,
                instead of the default behavior of verifying all pages
  -l PAGE     verify all pages linking to only the give PAGE,
                instead of the default behavior of verifying all links
  -h          print this help message and quit

Use this script against translated Archwiki pages downloaded by the
the 'arch-wiki-docs' software.

Report bugs to <rafaelff@gnome.org>

EOM
}

die() {
    echo "$1" 2>&1
    exit 1
}

# default behavior: search in all pages for links for any page.
searchall() {
    local pages=$1
    
    for page in $pages; do
        for linkedpage in $pages; do
            test_and_print
        done
    done
}

# behavior of -p option: search in a given page for links for any page.
searchpage() {
    local pages=$1
    local page=$2
    
    [ ! -f "$page" ] && die "No such page $page"
    
    for linkedpage in $pages; do
        [ "$page" == "$linkedpage" ] && break
        test_and_print
    done
}

# behavior of -l option: search in all pages for a given linked page.
searchlinkedpage()
{
    local pages=$1/
    local linkedpage=$2
    
    [ ! -f "$linkedpage" ] && die "No such page $linkedpage"
    
    for page in $pages; do
        [ "$page" == "$linkedpage" ] && break
        test_and_print
    done
}

# verify existence of a link to linked page $l in the page $p and,
# if it exists, run tests of known links that should not be reported.
# Will only output the link information if link exists and not listed
# below
test_and_print() {
    # use a shorter variable
    p=$page;  l=$linkedpage;  result=0

    if grep -qR "../en/$l" "$p"; then

        # aligned by page $p  and linkedpage $l
        [[                                                $l == "ArchWiki:About.html"             || \
            $p == "$l"                                                                            || \
            $p == "Table_of_contents.html"                                                        || \
           ($p == "Arch_Linux_press_coverage.html"     && $l == "Arch_Linux_press_coverage.html") || \
           ($p == "Trusted_Users.html"                 && $l == "Trusted_Users.html")
        ]] && result=1
        [ $result -eq 0 ] && echo "$p  -> ../en/$l"
    fi
}

# Set initial values.
p=0; l=0; dir=""; page="";

# Parse the command line.
OPTIND=1
while getopts "hvd:p:l:" opt; do
  case "$opt" in
    h ) usage; exit 0 ;;
    v ) version; exit 0 ;;
    d ) dir="$OPTARG" ;;
    p ) page=$OPTARG; p=1 ;;
    l ) page=$OPTARG; l=1 ;;
    * ) die "Invalid option" ;;
  esac
done
shift $((OPTIND-1))

# Get current directory, if not provided.
dir=${dir:-$(pwd)}

[ ! $p ] && [ ! $l ] && die "-p and -l are mutually exclusive."
[ ! -d "$dir" ] && die "No such directory: $dir"

cd "$dir" || exit 1

pages=$(find ./ -name '*.html' | sed 's|./||')
[ -z "$pages" ] && die "No HTML pages found in root directory \"$dir\"."

echo "*** Working dir: $dir ***"

[ $p -eq 1 ] && searchpage "$pages" "$page"
[ "$l" -eq 1 ] && searchlinkedpage "$pages" "$page"
searchall "$pages"

#!/bin/bash
#
#    django-compendium - Pull translations from Tx and create a compendium
#    Copyright (C) 2021 Rafael Fontenelle <rafaelff@gnome.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>


set -e

COMPENDIUM=true
REPOUPDATE=true
TXPULL=true
ROOTDIR="$(pwd)"
LANGUAGE=pt_BR

usage() {
    cat 1>&2 <<EOM
Usage: ${0##*\/} [OPTIONS]
Pull latest translations from Django projects in Transifex and create
a compendium for ease the use of grep commands.

OPTIONS:
  -c          disable compendium generation (default: generate compendium)
  -r          disable update of the repositories (default: update repos)
  -t          disable pulling translations for Transifex (default: do pull)
  -d DIR      root directory to run this script from (default: current dir)
  -l LANG     specify lang code to pull translations (default: $LANGUAGE)
  -h          print this help message and quit

Report bugs at https://gitlab.com/rafaelff/scripts

EOM
    exit 1
}


while getopts "crtd:l:h" arg; do
  case $arg in
    c)
      COMPENDIUM=false
      ;;
    r)
      REPOUPDATE=false
      ;;
    t)
      TXPULL=false
      ;;
    d)
      ROOTDIR="${OPTARG}"
      ;;
    l)
      LANGUAGE="${OPTARG}"
      ;;
    h)
      usage
      ;;
    *)
      usage
      ;;
  esac
done
shift $((OPTIND-1))

# django-docs-translations use specific branch for contributions
projects=(
    'django                                  https://github.com/django/django.git'
    'djangopeople                            https://github.com/django/djangopeople.git'
    'django-contrib-comments                 https://github.com/django/django-contrib-comments.git'
    'django-docs-translations  stable/3.2.x  https://github.com/django/django-docs-translations.git'
    'django-formtools                        https://github.com/jazzband/django-formtools.git'
    'django-localflavor                      https://github.com/django/django-localflavor.git'
    'djangoproject.com                       https://github.com/django/djangoproject.com.git'
)


test -d "$ROOTDIR" || ( echo "'$ROOTDIR' is not a valid directory"; exit 1; )
test -x /usr/bin/tx || ( echo "Transifex Client not installed"; exit 1; )
test -x /usr/bin/git || ( echo "Git not installed"; exit 1; )


# Phase: clone, pull-rebase and tx pull
for project in "${projects[@]}"; do
    dir=${project%% *}
    url=${project##* }
    
    unset branch usebranch
    if [[ "$dir" == "django-docs-translations" ]]; then
        branch=$(echo "$project" | awk '{print $2}')
        usebranch="-b $branch"
    fi
    
    cd "$ROOTDIR"
    
    if ! test -d "$dir"; then
        echo "'$dir' not found, cloning from $url ..."
        # shellcheck disable=SC2086
        git clone $usebranch "$url"
        cd "$dir"
    elif $REPOUPDATE; then
        cd "$dir"
        git checkout .
        [[ $dir == "django-docs-translations" ]] && git checkout "$branch"
        git pull --rebase
    else
        cd "$dir"
    fi
    
    if $TXPULL; then
        tx pull -l "$LANGUAGE" --parallel
    fi
done


# Phase: compendium
( ! $COMPENDIUM ) && exit

cd "$ROOTDIR"
pofiles=$(find . -type f -name '*.po' | grep locale/pt_BR/LC_MESSAGES/django.po)
# shellcheck disable=SC2086
pocompendium --correct compendium.po $pofiles
msgcat --no-wrap compendium.po > tmp
mv tmp compendium.po
echo "compendium.po created"

#!/bin/sh
# lysetup.sh - Set up LilyPond environment for building in Arch
#
# To clean up the build environment:
#    make distclean && git checkout python/ scripts/ configure.ac make/website.make
#
# To set up and build Lilypond program and documentation
#    lysetup.sh  &&  make -j$(($(nproc)+1))  &&  make -j$(($(nproc)+1)) doc
#
# Quick link to LilyPond web v2.21:
#    http://lilypond.org/doc/v2.21/Documentation/web/
#
####

# Language code supported by LilyPond
langcode=pt

# Require dependencies to be installed, quit showing missing ones.
# NOTE: from AUR: extractpdfmark texi2html1.82
pacman -Q dblatex extractpdfmark fontforge guile1.8 tex-gyre-fonts \
          texlive-bibtexextra texlive-langcyrillic texi2html1.82   \
          make netpbm python t1utils tidy                          \
          > /dev/null || exit 1

# LILYPOND_GIT must be set (e.g. to ~/lilypond-git in ~/.profile)
[ -n "$LILYPOND_GIT" ] || exit 1
cd "$LILYPOND_GIT" || exit 1

# Remove version dependency, because Arch has fontforge newer than 20110222
sed -i '/FONTFORGE, fontforge, REQUIRED/s/, 20110222//' configure.ac

# Fix Arch's python 2.7 binary name
export PYTHON="python2"
export PYTHON_CONFIG="python2-config"
find . -name '*.py' -type f -exec sed -ri 's|^#!/usr/bin/(env )?python$|&2|' '{}' \;
sed -e 's|^  PYTHON=python$|&2|' -i make/website.make

# Fix Arch's guile 1.8 binary name
export GUILE=/usr/bin/guile1.8
export GUILE_CONFIG=/usr/bin/guile-config1.8

# Use texi2html 1.82, instead of default version 5.0
sed -i 's/texi2html,/texi2html1.82,/' configure.ac
sed -i 's/TEXI2HTML_PROGRAM=texi2html$/&1.82/' make/website.make
sed -i '/TEXI2HTML=/s/"texi2html"/"texi2html1.82"/' \
    scripts/auxiliar/doc-section.sh                 \
    scripts/auxiliar/cg-section.sh

# Use the proper number of CPU currently available
echo "CPU_COUNT = $(nproc)" > local.make

# Enable only Portuguese to reduce buildtime (do not commit this!)
sed -i "/LANGUAGES = /s/(site, .*/(site, ${langcode})/" python/langdefs.py

./autogen.sh --noconfigure=1 && ./configure

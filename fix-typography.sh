#!/bin/bash
#    fix-typography.sh - apply GNOME HIG recomendations on Unicode
#                        chars to PO files, and open a graphical
#                        diff viewer.
#
#    Copyright (C) 2012-2019 Rafael Fontenelle <rafaelff@gnome.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
#
# See:     https://developer.gnome.org/hig/stable/typography.html
#
# Summary: “ ” ∶ × … ‘ ’ • –

die() {
    echo "$1" 2>&1
    exit 1
}

[ $# -eq 0 ] && die "No argument provided, expected a GNU Gettext PO file."
[ ! -f "$1" ]  && die "$1 must be a valid GNU Gettext PO filename."
[[ "$1" != *.po ]] && die "Filename is not a GNU Gettext PO, but a PO is expected."

[ ! -x "$(command -v msgcat)" ] && die "Gettext not installed but required."
[ ! -x "$(command -v meld)" ] && die "Meld not installed but required."
[ ! -x "$(command -v sed)" ] && die "Sed not installed but required."

oldpo=$1
newpo=${oldpo}-new
tmpfile=$(mktemp)

cp -a "$oldpo" "$newpo"

msgcat --no-wrap "$newpo" > "$tmpfile"
mv "$tmpfile" "$newpo"

# ratio (time)
# TODO: improve
#sed -i "$newpo"                         \
#    -e '/^msgid/s/:/∶/g'

# multiplication sign
# TODO: implement x->× replacement between numbers

# bullet
sed -i "$newpo"                         \
    -e '/^msgstr/s/^\"- /\"• /'         \

# en dash
sed -i "$newpo"                         \
    -e '/^msgstr/s/ - / – /'            \

# horizontal ellipsis
sed -i "$newpo"                         \
    -e '/^msgstr/s/\.\.\./…/g'          \

# right single quotation mark (apostrophe)
sed -i "$newpo"                         \
    -e "/^msgstr/s/'s/’s/g"             \
    -e "/^msgstr/s/s'/s’/g"             \

# left double quotation mark
sed -i "$newpo"                         \
    -e '/^msgstr/s/ \\"/ “/g'           \
    -e '/^msgstr/s/^\"\\"/\"“/g'        \
    -e 's/^msgstr \"\\"/msgstr \"“/g'   \
    -e '/^msgstr/s/(\\"/(“/g'           \

# right double quotation mark
# FIXME: wrongly replaces double quotes inside HTML link tags
sed -i "$newpo"                         \
    -e '/^msgstr/s/\\" /” /g'           \
    -e '/^msgstr/s/\\"\"$/”\"/g'        \
    -e '/^msgstr/s/\\":/”:/g'           \
    -e '/^msgstr/s/\\"∶/”∶/g'           \
    -e '/^msgstr/s/\\";/”;/g'           \
    -e '/^msgstr/s/\\",/”,/g'           \
    -e '/^msgstr/s/\\"\./”\./g'         \
    -e '/^msgstr/s/)\\"/)”/g'           \
    -e '/^msgstr/s/\\")/”)/g'           \
    -e '/^msgstr/s/\\"(/”(/g'           \
    -e '/^msgstr/s/\\"?/”?/g'           \
    -e '/^msgstr/s/\\"\\n/”\\n/g'       \
    -e '/^msgstr/s/\\"…/”…/g'           \
    -e '/^msgstr/s/\\"!/”!/g'

msgcat "$newpo" > "$tmpfile"
mv "$tmpfile" "$newpo"

msgcat "$oldpo" > "$tmpfile"
mv "$tmpfile" "$oldpo"

meld "$oldpo" "$newpo"


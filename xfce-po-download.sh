#!/bin/bash
#
#    xfce-po-downloads - download translations from Xfce repository
#    Copyright (C) 2018-2024 Rafael Fontenelle <rafaelff@gnome.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>

DL_URL=https://git.xfce.org
LANGUAGE=pt_BR
ROOTDIR=$HOME/l10n
PODIR=xfce-po-files
OUTFILE=xfce-translations

die() {
    echo "$1" >&2
    exit 1
}

for binary in aria2c msgcat msgattrib po2tmx; do
    if [ ! -x "$(command -v $binary 2> /dev/null)" ]; then
        die "$binary not found but required. Please install it. Exiting..."
    fi
done

echo -e "Let's get Xfce PO files!"

if [ ! -d "$ROOTDIR" ]; then
    mkdir "$ROOTDIR" || die "Unable to make and enter the rootdir $ROOTDIR"
fi

if [ -d "$ROOTDIR"/$PODIR ]; then
    rm -rf "${ROOTDIR:?}"/$PODIR
fi

mkdir -p "$ROOTDIR"/$PODIR
cd "$ROOTDIR"/$PODIR || die "Unable to enter the podir $PODIR (rootdir $ROOTDIR)"

rm -f $OUTFILE.po*
rm -f ./*.html*
rm -f ./*.tmx*
rm -f todownloadlist

# (e.g. https://git.xfce.org)
wget -q $DL_URL -O $LANGUAGE-page1.html
wget -q $DL_URL/?ofs=150 -O $LANGUAGE-page2.html

# Filter the HTML pages for the URLs of the software repositories that might
# contain PO files to download. What does this line below do, in order:
#
# 1- Gets only lines containing a repository (only repository lines have
#    "class='sublevel-repo'").
# 2- Removes every content in the line except for a part containing href
#    for the summary of software repository ("summary" is a clickable hyperlink
#    in the page).
# 3- Filters for the 4th column, which should be the URL we want.
# 4- Removes leading and trailing bars for cosmetic reason further on
# 5- Excludes projects known to not have translation files or that are not
#    available in the translation platform (Transifex, currently)
# 6- Filters the repositories we don't want to grep stuff from.
#
# Extra (outside the command pipeline, adds www.xfce.org manually as it is
# the only repository from 'www' reposection that has translation files
mapfile -t pofilesurl < <(grep "class='sublevel-repo'" $LANGUAGE-page{1,2}.html |   \
                          sed 's|.*\(</td><td><a.*summary\).*|\1|' | cut -d\' -f4 | \
                          sed 's|^/||;s|/$||' |                                     \
                          grep -Ev 'xfce4-dev-tools|xfce4-volumed-pulse|xfdashboard|xfce4-generic-slider' | \
                          grep -Ev 'users/|bindings/|archive/|art/|www/')
                          
# TODO Adicionar www/www.xfce.org !

rm $LANGUAGE-page{1,2}.html

touch todownloadlist
# shellcheck disable=SC2048
for po_url in ${pofilesurl[*]}; do

    # handle 'www.xfce.org' different po directory.
    po_dir='po'
    [[ $po_url == 'www/www.xfce.org' ]] && po_dir='lib/po'

    echo "$DL_URL/$po_url/plain/$po_dir/$LANGUAGE.po" >> todownloadlist
    echo " out=$(echo "$po_url" | cut -d/ -f2).$LANGUAGE.po" >> todownloadlist
done
aria2c -i todownloadlist

echo "Verifying the need for converting charset to UTF-8... "
# shellcheck disable=SC2048
for pofile in ${pofilesurl[*]}; do
    encoding=$(file -bi "$pofile" | sed -e 's/.*[ ]charset=//')
    if [ "$encoding" == iso-8859-1 ]; then
        echo "    converting $pofile..."
        iconv -f iso-8859-1 -t utf-8 "$pofile" > "$pofile-aux"
        mv "$pofile-aux" "$pofile"
        sed -i '/Content-Type/s/ISO-8859-1/UTF-8/' "$pofile"
        sed -i '/Content-Type/s/iso-8859-1/UTF-8/' "$pofile"
    fi
done

echo "Applying no-wrap lines... "
for po in ./*.po; do
    msgcat --no-wrap "$po" > tmp && \
    mv tmp "$po"
done

echo "Combining PO files into one big PO file... "
msgcat ./*.po | \
msgattrib --no-fuzzy --no-obsolete --translated --no-wrap --no-location -o "$ROOTDIR"/$OUTFILE.po

echo "Cleaning header and set proper header the the big PO file... "
sed -i '0,/^$/d' "$ROOTDIR"/$OUTFILE.po    # cleanup header lines
cp "$ROOTDIR"/$OUTFILE.po{,-bkp}
cat > header-file << EOM
msgid ""
msgstr ""
"Project-Id-Version: $OUTFILE\n"
"POT-Creation-Date: $(date -u +'%F %T%z')\n"
"PO-Revision-Date: $(date +'%F %T%z')\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: ${LANGUAGE} <LL@li.org>\n"
"Language: ${LANGUAGE}\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

EOM
cat header-file "$ROOTDIR"/$OUTFILE.po-bkp > "$ROOTDIR"/$OUTFILE.po
rm header-file "$ROOTDIR"/$OUTFILE.po-bkp

# Provides faster importing to Machine Translation
echo "Converting to TMX... "
po2tmx --language=$LANGUAGE -i "$ROOTDIR"/$OUTFILE.po -o "$ROOTDIR"/$OUTFILE.tmx

echo "$OUTFILE.po is stored in $ROOTDIR"

#!/bin/bash
# 
# licencas-sl-po-download - get translations files from licencas.gitlab.io
#
# Copyright (C) 2018-2020 Rafael Fontenelle <rafaelff@gnome.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#

LICENCASDIR=$HOME/repos/licencas.gitlab.io
ROOTDIR=$HOME/l10n
PODIR=licencas-sl-po-files
OUTFILE=licencas-sl-translations

die() {
    echo "$1" >&2
    exit 1
}

if [ -f "$LICENCASDIR"/.git/config ]; then
    if ! grep -q 'licencas/licencas.gitlab.io' "$LICENCASDIR"/.git/config; then
        die "'$LICENCASDIR' doesn't seem to be a valid licencas.gitlab.io Git repository"
    fi
else
    mkdir -p "$(dirname LICENCASDIR)"
    git clone --depth 1 https://gitlab.com/licencas/licencas.gitlab.io "$LICENCASDIR"
    [ $? -ne 1 ] && die "Clone failed to '$LICENCASDIR'"
fi

if [ ! -d "$ROOTDIR" ]; then
    mkdir "$ROOTDIR" || die "Unable to make and enter the rootdir $ROOTDIR"
fi

if [ -d "$ROOTDIR"/$PODIR ]; then
    rm -rf "${ROOTDIR:?}"/$PODIR
fi

mkdir -p "$ROOTDIR"/$PODIR
cd "$ROOTDIR"/$PODIR || die "Unable to enter the podir $PODIR (rootdir $ROOTDIR)"

echo -e "Let's get GNU licenses PO files!"

src_po_files="$(find "$LICENCASDIR" -type f -name '*.pt-br.po')"

for src_po in $src_po_files; do
    dest_po=${src_po##*/}
    cp "$src_po" "$dest_po"
done

echo "Combining PO files into one big PO file... "
msgcat ./*.po | \
msgattrib --no-fuzzy --no-obsolete --translated --no-wrap --no-location -o "$ROOTDIR"/$OUTFILE.po

echo "Cleaning header and set proper header the the big PO file... "
sed -i '0,/^$/d' "$ROOTDIR"/$OUTFILE.po    # cleanup header lines
cp "$ROOTDIR"/$OUTFILE.po{,-bkp}
cat > header-file << EOM
msgid ""
msgstr ""
"Project-Id-Version: $OUTFILE\n"
"POT-Creation-Date: $(date -u +'%F %T%z')\n"
"PO-Revision-Date: $(date +'%F %T%z')\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: ${LANGUAGE} <LL@li.org>\n"
"Language: ${LANGUAGE}\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

EOM
cat header-file "$ROOTDIR"/$OUTFILE.po-bkp > "$ROOTDIR"/$OUTFILE.po
rm header-file "$ROOTDIR"/$OUTFILE.po-bkp

# Provides faster importing to Machine Translation
echo "Converting to TMX... "
po2tmx --language=pt_BR -i "$ROOTDIR"/$OUTFILE.po -o "$ROOTDIR"/$OUTFILE.tmx

echo "$OUTFILE.po/.tmx are stored in $ROOTDIR"

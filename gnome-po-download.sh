#!/bin/bash
#
# gnome-po-download - Get source of GNOME translatable modules from Damned Lies
# (Based on mt.sh by Daniel Mustieles García from ES lang team from GNOME)
#
# Copyright (C) 2018-2024 Rafael Fontenelle <rafaelff@gnome.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#

VERSION="gnome-47"
LANGUAGE=pt_BR
DL_URL="https://l10n.gnome.org/languages/$LANGUAGE"
ROOTDIR=$HOME/l10n
PODIR=gnome-po-files
OUTFILE=gnome-translations

die() {
    echo "$1" >&2
    exit 1
}

for binary in aria2c msgcat msgattrib po2tmx wget; do
    if [ ! -x "$(command -v $binary 2> /dev/null)" ]; then
        die "$binary not found but required. Please install it. Exiting..."
    fi
done

if [ ! -d "$ROOTDIR" ]; then
    mkdir "$ROOTDIR" || \
        die "Unable to make and enter the rootdir $ROOTDIR"
fi

if [ -d "$ROOTDIR"/$PODIR ]; then
    rm -rf "${ROOTDIR:?}"/$PODIR
fi

mkdir -p "$ROOTDIR"/$PODIR
cd "$ROOTDIR"/$PODIR || die "Unable to enter the podir $PODIR (rootdir $ROOTDIR)"

rm -f $OUTFILE.po*  $OUTFILE.tmx  todownloadlist

trap "rm todownloadlist" 0

echo "Let's get GNOME PO files for version $VERSION!"

DL_LIST=("$VERSION"/ui
         "$VERSION"/doc
         incubator/ui
         incubator/doc
         gnome-circle/ui
         gnome-circle/doc
         gnome-infrastructure/ui
         gnome-infrastructure/doc
         evolution/ui
         evolution/doc
         gnome-gimp/ui
         gnome-gimp/doc
         librem5/ui
         gnome-extras/ui
         gnome-extras/doc
         freedesktop-org/ui
         freedesktop-org/doc)

echo "Downloading PO files... "
touch todownloadlist
for file in "${DL_LIST[@]}"; do
    echo "$DL_URL/$file.tar.gz" >> todownloadlist
done
aria2c -i todownloadlist

# Files are redirected to another file name; extract the correct filename
echo "Extracting files..."
today=$(date -u +'%Y-%m-%d')
for file in "${DL_LIST[@]}"; do
    tarball=${file/\//.}.$LANGUAGE.$today.tar.gz
    tar -zxf "$tarball" && \
    rm "$tarball"
done

echo "Applying no-wrap lines... "
for po in ./*.po; do
    msgcat --no-wrap "$po" > tmp && \
    mv tmp "$po"
done

echo "Combining PO files into one big PO file... "
msgcat ./*.po | \
msgattrib --no-fuzzy --no-obsolete --translated --no-wrap --no-location -o "$ROOTDIR"/$OUTFILE.po

echo "Cleaning header and set proper header the the big PO file... "
sed -i '0,/^$/d' "$ROOTDIR"/$OUTFILE.po    # cleanup header lines
cp "$ROOTDIR"/$OUTFILE.po{,-bkp}
cat > header-file << EOM
msgid ""
msgstr ""
"Project-Id-Version: $OUTFILE\n"
"POT-Creation-Date: $(date -u +'%F %T%z')\n"
"PO-Revision-Date: $(date +'%F %T%z')\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: ${LANGUAGE} <LL@li.org>\n"
"Language: ${LANGUAGE}\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

EOM
cat header-file "$ROOTDIR"/$OUTFILE.po-bkp > "$ROOTDIR"/$OUTFILE.po
rm header-file "$ROOTDIR"/$OUTFILE.po-bkp

# Provides faster importing to Machine Translation
echo -n "Converting to TMX..."
po2tmx --language=$LANGUAGE -i "$ROOTDIR"/$OUTFILE.po -o "$ROOTDIR"/$OUTFILE.tmx

echo "$OUTFILE.po/.tmx are stored in $ROOTDIR"

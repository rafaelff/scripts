#!/bin/bash
#
#   setup-licenses - prepare GNU licenses for unofficial translation
#   Copyright (C) 2017-2019  Rafael Fontenelle
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

program=${0##*\/}

function usage () {
    cat << EOM
Usage: $program [<dir>|-h]

  This script downloads GNU licenses in HTML format,
  and prepare them for translation by prepending the
  "unofficial translation" notice from
  <https://www.gnu.org/licenses/translations.html>
  and appending a "translator's credit" section.

  The directory 'dir' is where the HTML files will
  be stored. Defaults to 'src' in current dir.
  If it doesn't exist, it will be created.

Report bugs to <rafaelff@gnome.org>
EOM
}

 # downloads the license body, and concatenates into one HTML for translation
function setup_html() {

    path=$1
    longname=$2
    shortname=$3
    licensename=${path/old-licenses\/}

    type=software
    if [[ "$shortname" == "GNU FDL" ]]; then
        type=documentation
    fi

    echo "Setting up $licensename ... "

    if wget -qNP "$dir" "https://www.gnu.org/licenses/$path-body.include"; then
        echo "Failed to download $licensename; ignoring."
    else
        
        # Here starts a 4-part redirection to file "$dir/$licensename.html"
        {
        
        # Part 1 of the HTML file of the license: unofficial translation notice
        cat << EOM
<blockquote><p dir="ltr" style="text-align:left">
<!--TRANSLATORS: If you translate this paragraph, retain the English
text and put your translation below.  The entire <blockquote> element
has <p dir="ltr" style="text-align:left"> to work by default with RTL
languages when the notice is not translated, so if your language is RTL,
you *must* put your translation within
</p><p dir="rtl" style="text-align:right">......</p><p>-->
This is an unofficial translation of the ${longname}
into <tt>language</tt>.  It was not published by the Free Software
Foundation, and does not legally state the distribution terms for
${type} that uses the ${shortname}&mdash;only the original English text of
the ${shortname} does that.  However, we hope that this translation will
help <tt>language</tt> speakers understand the ${shortname} better.
</p></blockquote>


EOM

        # Part 2 of the HTML file of the license: publishing permission
        cat << EOM
<blockquote><p dir="ltr" style="text-align:left">
<!--TRANSLATORS: If you translate this paragraph, retain the English
text and put your translation below.  The entire <blockquote> element
has <p dir="ltr" style="text-align:left"> to work by default with RTL
languages when the notice is not translated, so if your language is RTL,
you *must* put your translation within
</p><p dir="rtl" style="text-align:right">......</p><p>-->
You may publish this translation, modified or unmodified, only under
the terms at <a href=http://www.gnu.org/licenses/translations.html>
http://www.gnu.org/licenses/translations.html</a></p>
</p></blockquote>


EOM

        # Part 3 of the HTML file of the license: the main part of the license
        cat "$dir/$licensename-body.include"

        # Part 4 of the HTML file of the license: translator credtis
        cat << EOM

<hr>

<p>
<!--TRANSLATORS: Your name and email goes here. Use SPACE for none-->
TRANSLATOR-CREDITS
</p>
EOM

    # Finish redirection
    } >> "$dir/$licensename.html"
        
        echo "done."
        
    fi

} # close setup_html


if [ $# -gt 1 ]; then
    echo "Expected at most one argument. See \"$program -h\"."
    exit 1
fi

if [ $# -eq 1 ]; then
    case "$1" in
        -h | --help ) usage; exit 0 ;;
        -* ) echo -e "Invalid option \"$1\".\n"; usage; exit 1 ;;
        * ) dir="$1" ;;
    esac
else
    dir="$(pwd)/src"
fi
[ -d "$dir" ] || mkdir -p "$dir" || exit 1
[ -w "$dir" ] || (echo "\"$dir\" is not writable"; exit 1)

echo "workdir = $dir"

setup_html 'agpl-3.0'                'GNU Affero General Public License'       'GNU AGPL'
setup_html 'old-licenses/fdl-1.1'    'GNU Free Documentation License'          'GNU FDL'
setup_html 'old-licenses/fdl-1.2'    'GNU Free Documentation License'          'GNU FDL'
setup_html 'fdl-1.3'                 'GNU Free Documentation License'          'GNU FDL'
setup_html 'old-licenses/gpl-1.0'    'GNU General Public License'              'GNU GPL'
setup_html 'old-licenses/gpl-2.0'    'GNU General Public License'              'GNU GPL'
setup_html 'gpl-3.0'                 'GNU General Public License'              'GNU GPL'
setup_html 'old-licenses/lgpl-2.0'   'GNU Library General Public License'      'GNU LGPL'
setup_html 'old-licenses/lgpl-2.1'   'GNU Lesser General Public License'       'GNU LGPL'
setup_html 'lgpl-3.0'                'GNU Lesser General Public License'       'GNU LGPL'
setup_html 'autoconf-exception-3.0'  'GNU Autoconf Configure Script Exception' 'Exception'
setup_html 'gcc-exception-3.0'       'GCC Runtime Library Exception'           'GCC RLE'
setup_html 'gcc-exception-3.1'       'GCC Runtime Library Exception'           'GCC RLE'
